from datetime import timedelta

from prefect import flow, task
from prefect.blocks.system import String
from prefect.tasks import task_input_hash

name_block = String.load("secret-name")


@task(version='3.3.3', cache_key_fn=task_input_hash, cache_expiration=timedelta(minutes=1))
def create_message():
    return f'Hello {name_block.value} from Task'


@task(version='2.2.2')
def print_message(msg):
    print(msg)


@flow
def sub_flow1():
    pass


@flow(version='1.0.1', retries=1, name='cool_world')
def hello_world():
    sub_flow1()
    print_message(create_message())


if __name__ == '__main__':
    hello_world()
